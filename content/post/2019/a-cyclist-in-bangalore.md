---
date: '2019-01-06'
description: What is your favourite superpower? How did you stumble upon yours?
tags:
  - bangalore
  - cycling
  - commute
  - traffic
  - satire
  - sarcasm
title: a cyclist in Bangalore
---


When I was young, one of the superpowers I wanted was invisibility. This is a wish that hasn't changed in decades. 'What would you do with the power?', I would be asked. And I would smile in response. The asker would simply assume (and sometimes shout out) the worst---cheating at hide-and-seek to voyeurism. But it didn't matter.

Years later---or rather, last month---I got a chance to travel to Varanasi. There, I met a mystique from Bangalore. It was great to be able to hear Kannada hundreds of kilometres away from home. He found me looking in his direction, and beckoned to me. I am a nice guy, so, I went to him smiling. Smiling back, he said, 'You have not come here without reason.' Cold reading gone wrong. My smile vanished. 'Actually, I just came here with a friend. _He_ had some business here,' I said. The mystique wasn't the kind to let go. (Bangaloreans!) 'Nobody is without desires. Talk to me.' I gave in to the temptation. (Bangaloreans!) What did I have to lose?

'All right. I want the ability to be invisible.'

The mystique's smile widened. 'Ah, a simpleton,' he declared. Sarcasm? He fished out a little piece of paper from his foot-deep pocket and wrote on it an address. And he vanished. (Before you ask, I don't know how. And I didn't care; I would be able to do it soon, too, anyway.) I unfurled the slip and read what was written. It was an address. In Bangalore! Truly, there was nothing you could not get here.

We left Varanasi the next day. As soon as we hit the tarmac back at home, I pulled out my bag from the overhead compartment and rushed to the front of the aircraft, before everyone else. {{< smallcaps "BMTC" >}} had trained me well.

Five hours later, I reached the mentioned address. It was in Jayanagar, close to home---I mean, what are the odds! The problem was, I'd forgotten to ask the mystique what the code word was. How could I have been so stupid?

But I am an engineer. I wasn't about to let go just yet. I walked into the place to be greeted by a skinny man in yellow. He asked me how he could help me. I tried the straight arrow of innocence: 'Umm ... I'm looking for someone who could help me with ... invisibility.'

The man gave out a hearty laughter, and patted me on my shoulder, leading me into the store. 'Okay,' I thought, 'he is probably taking me to a secret chamber or something.' Before I finished the thought, he asked me, 'So, what features are you looking for?' I didn't quite know what to expect. I shrugged. 'Well, where does it all start?'

He nodded. 'Beginner, huh?' he asked. So apparently, there were pros in this. What would they be like? I nodded in response. 'This way', he said, and led me to the western side of the store. The evening sun was mild. A silver metallic frame caught my attention at once.

'Ah, you're looking for speed, I suppose?'

I didn't know what to respond. 'Err ... my expectations are pretty basic.'

'All right,' he said, as he swished past and lifted off the wall hook, a sexy, red-and-black bike. 'This should do. It's all-aluminium. Sixteen speeds, front disk, and rear caliper. Twenty-five&nbsp;mm tyres. Lightweight. You'll love it. Of course, there's carbon fibre as well, but start with this for now.'

What was he talking about? I get it: it's a bicycle store. But ... I had expressed no interest in buying a bike, right?

'Go on, try it out!'

This was one of the stores with great salesmen. I could not step out of the store without buying the bike. The guilt of walking out empty-handed was almost unbearable to even imagine. Perhaps he would give me the cloak only if I bought a bicycle? So, I bought the bike. And then, I reminded him about the cloak. He laughed again. 'You're funny, man!'

I felt embarrassed to ask him again. I decided to go home.

But as soon as I hit the road, everything started to make sense. Auto-rickshaws would cut me from within an inch. Buses would zip past, their side-winds pushing me away. Pedestrians would cross the road without so much as batting an eyelid. Cabbies would drive on both my sides---in parallel---and talk to each other. Cops wouldn't notice if I "jumped" a signal. I really did not exist in their world. I was thin (and fast) as air.

My whole life had been a lie. From fairy tales to Harry Potter, all the books and movies had taught me to think of an invisibility cloak as an actual cloak, made of cloth or something, that you could don and be invisible. Wrong. It was all metaphorical. Invisibility cloaks are actually made of metal and rubber and fibre. They have wheels and chains. And they have been widely available. Children use them the most. This phenomenon is never really noticed because kids ride in the little streets around their houses. The true nature of these cloaks can be seen only on roads---real roads. These things can bend light. Like a black hole, yes. And I ride a black hole with red accents.

But (and this is a big but): You may want to remember that you don't become plasma or anything when riding them. Meaning, you can still be touched (and hit) and all. People won't realise they've hit you until you stop. This invisibility has something to do with angular momentum, I guess. Anyway, I'm an engineer; I have no idea what physics is. So, yeah, peace out, yo. Take care. Go invisible; go green. Whatever.