---
date: '2019-02-10'
description: A story of a vada pav after he joins a corporate that helps him reach his dreams. What is the price he pays for it, though?
tags:
  - satire
  - corporate
  - technology
  - business
  - politics
  - ethics
  - loopholes
  - wordplay
  - manipulation
title: a corporate vada pav
---


I come from a humble background, from Maharashtra. I had hardly stepped out of my home town my entire childhood. But when I grew up, somehow, I was wanted. By so many people. By so many companies. When I told my kin about this, they were very happy. I was getting what my forefathers had only dreamt about. I was not only stepping out of my home town, but I was stepping into a whole new world, that somehow, seemed to celebrate me. From uncouth, home-bred _desi_-ness, I was all set to go international.

I completed my education. Right when I was out of the overused, dark oil, I was swept off my feet, in bulk, and placed in _Food Global Solutions_. That was the first time I realised food was a "global problem", and that my company had taken it upon itself to solve it. Soon, I also realised, "problem" meant something entirely different from what it seemed, in this new world---according to the Food Solution Intelligence Library (or {{< smallcaps "FSIL" >}}) V3. This document, as you would expect, was the Bible of food solutions. Every company that could hire the likes of us had to follow it. And be certified.

I was rustic, and spicy. I was told that I was picked for those exact qualities. These qualities apparently made me stand out as a great choice of food. Not to mention that I was an almost complete food item. I had enough carbs thanks to the bun and the potatoes. I had protein making up the outer shell of the vada. I had fat in the vada---from being deep-fried. I had a few other nutrients in the _lahsun chutney_. I tasted fantastic; people back in my hometown loved me. And when I came here, they loved me here, too. But they said, I needed some "refinement".

Over time, my superiors decided that I should lose the shell of my vada for something crunchier. So, I worked on getting that changed. They wanted me fried in clarified oil that stuck to its own set of "standards". I did that. Then someone said my spice level had to be toned down for international consumption. So, I became bland. Instead of turmeric, colour was used. My chutney powder was replaced with a wet chutney, which curiously had some Chinese salt in it.

My pav was now "slightly heated" in "clarified butter". My spices were changed. In short, I was no more the real me, but my company would sell me as vada pav. I was tuned, refined, and ... corporate-ised.

Then, there were also many variants of me, sold out to the world. There was me with spinach and corn filled in the patty, there was me with Schezhuan sauce, or with mayonnaise. "Customers like the feeling of choice", I was told. But as a vada pav, this is not what I wanted. I wanted to be a vada pav, and vada pav, to me, only meant one thing.

But then, I didn't mind that, much. People paid for me without complaining, I had a lot of perks---like medical insurance and all, I spent my days in air conditioned rooms, travelled by chartered vehicles, I was given all facilities necessary to become better, or more corporate-like, and all that. I was well cared for. I even got to meet foreigners! Fun fact: I hadn't even heard of this thing called medical insurance until I came here.

Life started becoming better and better as days passed. I now looked good, I mean, I was now proportionately built, I never looked dry, no uneven protrusions from my vada, I was always packed and presentable; even my good old partner, Mirchi, looked all slick and alive, with sprinkles of salt visible all over her! She looked delicious even to me! 'Presentation is half the story, my friend,' I was told by this gigantic, cheerful man who loved to pat my back, and laughed heartily.

We spent our days enjoying ourselves. We had fun activities on Fridays. We went to resorts to chill, once every quarter. I got gifts every year. I had enough money to buy things for the folks in my home town. They would celebrate my visits there in return. I was soon declared the _Ideal Vada Pav_. 'Every child should be like yours,' they would tell my mother. And whenever my mother heard that, her face would light up in happiness. My father would lift his head up and puff his chest in pride.

One day, I saw myself and Mirchi on a happy poster. We stood there among other happy food. People eating us were happy as well. It was this, 'Wow, we have _them_ by our side!' happiness (and not the 'Oh wow, I now have some food!' happiness---it was important we understood the difference). These posters, I heard, were displayed all over the world. But I didn't remember posing for pictures this way and all. Mirchi didn't, either, when I asked her. I went closer to the poster to take a better look. I didn't recognise myself over there, now. I looked good, but certainly not _this_ good. I looked delicious all right, but again, my vada was never big enough to look like a belly. And the vada was usually pressed into the pav---the two were inseparable, not so far apart.

I went into the poster. I wanted to find out what was going on. The guy who'd made the poster told me it was not a real vada pav. I didn't quite understand. I was told that the mayonnaise was actually glue. The patty was coated with a little acrylic to give that mild shine, that the patty was separated from the bun⁠---

Oh wait, why am I talking like them?! I meant that the vada was coated with a little acrylic to give that mild shine, that the vada was separated from the pav using a couple of sheets of padding to give a fuller look, they had used a few mods here and there to make me look more desirable. I was told that none of my colleagues were real, in there. What was going on? When I asked my most trusted friend, Mirchi, she said, 'Well, how does it matter? That's how business works. You show everything in a more desirable fashion. Stop making a big deal out of everything.' She rubbed some salt off of her arm.

I did not quite like my recent discovery, and what she said. That is exactly how a business should _not_ work. Whatever happened to reality? I wanted to ask my superiors about it. I got to work, only to find out that my superior was not available for the entire day. He was, apparently, on one-on-one meetings with other food items. I was told it was "appraisal" time. 'Okay,' I said, 'when do I get to meet him?' My superior's assistant scrolled around his screen and said, 'You know what? You have your appraisal meeting scheduled for tomorrow, but I think I can squeeze you in, in the next slot. Why don't you discuss this with Boss during the appraisal discussion?' I liked the idea. I nodded. He added, 'Very well, but a piece of advice, as your friend, don't push your thoughts too much. You don't want your rating going down because of that.'

'But it's a matter of what's right and wrong. It's a matter of ethics. It's something we never do to our customers!' I protested.

'You are absolutely right, there is no doubt in that. But just trust me on this. Look at your growth. Customers are fine. You are fine. Your bosses are fine. Everyone is happy. Just leave it that way. Don't ruffle too much. It's important you make your point, of course, but don't jeopardise your career over things that don't affect you directly. Especially when there are others waiting to take your position. You are here to grow. Just concentrate on that. Don't try to change the entire system.'

When I went in for the discussion, I saw that my boss had changed. Or I had entered the wrong room. 'Ah, Vada Pav, the star; come in!' The boss man greeted me with a fake, lopsided smile. I wasn't in the wrong room, but I certainly was with a wrong person in the right room.

'Umm, hello, sir.' I greeted him. In a low, uncertain voice, I asked him, 'I thought I was going to have a discussion with Mr Burger?'

'Ah, Mr Pav, I'm afraid you are going to have to make do with me. Burger left the company for a better opportunity. I know, I didn't see it coming, either. But it's a good thing, trust me. We are going to make the best out of what we have with us.'

I nodded.

'Tell you what,' my boss declared, 'why don't you take up Burger's role? I could promote you this cycle. You'll obviously get a handsome hike as well. It is better we handle this within the company rather than getting an outside talent for it, don't you think?'

I didn't quite understand what he meant. Mr Burger was a burger, and I am a vada pav. How can one do the role of another?

'Are you wondering if you would be able to do justice to the role?'

'Roles, sir. How would I be doing justice to my role by ditching it and doing something entirely different from what I am designed for?'

'But my friend, the client is comfortable with Burger. And he needs Burger. His entire business depends on it. You have been with Burger long enough to know what he did, and I know you're talented enough to pass off as Burger given a little up-skilling.'

'Excuse me, "up"-skilling?'

'Okay, call it "cross"-skilling if up-skilling disturbs you. It doesn't have to, because I just meant in terms of the role---like increase the number of your skills, not that your skills were inferior.'

I nodded.

'Look, political correctness aside, I want you to⁠---'

'Sir, with all due respect, we are two different people doing two different things, with two very different sets of characteristics.'

'Oh, come on. You two are not that very different. You have a patty, Burger has a patty. You have a bun split into two, so does Burger. Your patty is fried, Burger's patty could be anything from barbecued meat to soaked quinoa. In fact, I would daresay that you would be enjoyed by significantly more people than you are currently, but you would need to become more flexible in terms of size, you would need a few sesame seeds on your head, and be a little sweeter and more flexible in terms of sauces. People skills! But given these, you can easily pass off as Burger. You do this well, and you will be judged well in your next appraisal. You screw this up, and your career as you know it is over. But come on, you're a star performer; you shouldn't be giving up at this stage, where I'm opening more doors than you have seen in your entire life. Don't back out like a loser, my friend. Seize the opportunity. Travel out. Meet more people.'

'But, what happens to my role? What happens to customers who love a hot vada pav?'

'Don't worry, my friend, we'll promote Mangalore Bun and Mysore Bonda as a combo. No customer refuses a combo. Besides, you are sold at sixty bucks a pop⁠---'

'Sixty? I was being paid five!'

'Oh, I know. Think about the perks you get.'

'But that still⁠---'

My boss continued, ignoring me, 'These two guys, even put together, shouldn't cost me as much as you do. Better profit. And the guys will be more than happy to take up your role. I've already had a word with them⁠---'

'But sir, they are not Vada Pav!'

'Of course, they're not, buddy. Nobody can replace you⁠---'

'Sir, what about clients⁠---'

'They're foreigners, man. They have no idea about what the duck a vada pav is. They're still happy eating you; are you the real deal anymore? If we tell them Mangalore Bun and Mysore Bonda could be Vada Pav when put together, they will buy it. If not, we'll present them with a case study in history speculating if Vada Pav in fact is actually a spiced version of Mangalore Bun and Mysore Bonda.

'But that would be a lie!'

'Not if we're merely speculating. Come on, you've been here long enough to know all this. Grow up, buddy boy. See, nobody reads the entire case study and all. It's merely paperwork for a bad day when we have an audit. I know how to compose the email to make it read in my favour.'

I was surprised I was even hearing all this.

'You think about it. Take your time. Don't give me surprises, though. Surprises are my thing.' He smiled. 'But hey, we're not here to force anyone to do anything, right? We don't hold anyone in what they don't like doing. You're obviously free to choose what you like. You're even free to leave if you feel so inclined. Just ... keep me informed. This place is all about freedom. But remember, you are here to grow. You go to the next level if you show, being in the current role, that you're ready for the next. And today, clients expect a different level of flexible. They want multi-skilled foods. Times are changing. Corners are being cut. There is pressure from above to reduce my costs. If you can also do what Burger used to, and at the same time, mentor Mangalore Bun and Mysore Bonda to become you, you are my next favourite. I'll ensure you get a hundred percent hike; it should be no problem, considering how much Burger used to make. Let's have a win-win.'

Later that day, I overheard Mr&nbsp;Bossman talk to my client, 'Yeah, there's not much of a difference, anyway, Shane. We found a good skill overlap in Vada Pav. He should be taking up Burger's role in no time. Business will⁠---'

I couldn't hear beyond that; the door had closed by then.

---

The inspiration for this was a statement my brother made when we were laughing at our own plight as employees, in an attempt to take on life in good humour.