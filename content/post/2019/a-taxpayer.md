---
date: '2019-01-13'
description: Paying tax is part of the middle-class life. Are you happy the way your tax is used? If not, are you happy that you are taxed?
published: true
tags:
  - tax
  - farmer
  - employee
  - landlord
  - middle-class
title: a taxpayer
---


My history class introduced me to taxes, the rulers' "unfair" means of earning. My mathematics class introduced me to the modern system of Income Tax. I did not like either version of it. Subliminal messages from my taxpaying teachers?

But I was also told that there were certain---legal---ways by which I could avoid tax. This would be useful later.

In the senior grades, I picked the Science stream and completely lost connection with understanding the way an economy ran. All I now knew was paying taxes was a business deal with the government; the deal dictated that the government did a lot for me, the citizen, such as giving me good roads, cheap food, medical facilities, free education, and a plethora of other things that you hear in a political manifesto; in return, I had to share a percentage of my income with the government. Collectively, we "ran one of the largest economies of the world".

However, sometimes the system felt unfair. Sometimes, it felt stupid.

I mean, think about it. The government and government-run agencies taught me how to escape tax! There were banks and other financial institutions that openly advertised "tax-saving schemes"; schemes almost took a different meaning according to me. And some "schemes" angered me. The statistics added fuel to it.

I felt like crying looking at my payslip. Eighteen percent of it went out as Tax Deducted at Source (dearly known as {{< smallcaps "TDS" >}}).

I own a house in which I don't live. I have rented it out. I bought the house in a metropolitan city that is not recognised as a metropolis. I bought it at seven million. The papers would lie that I paid four-point-two. This being one of the initial years, the principal I pay back monthly is a measly six thousand rupees, whereas over twenty-seven thousand is the interest. I cannot claim tax exemption on the interest. The rent I fetch from the house is a fourth of the {{< smallcaps "EMI" >}} (bank loan and gold mortgage put together). But after tax, I would receive only a fifth.

So my tenant and I have agreed on a few terms: The tenant would only transfer the rent to me; he would pay the maintenance charges directly to the association. On one month, the rent will be sent to my account; on the other, it would be transferred to my wife's, and the next to my son's and so on. On one month, the rent would be five hundred rupees less; on the next, five hundred more. This way, the accounts will show no pattern.

I haven't given my {{< smallcaps "PAN" >}} details to my tenant. Why should I? He will get a tax exemption---no matter how miniscule---but I would come under the radar. The tax saving for me would be some four thousand rupees; for him, it would just be a thousand. I might as well reduce half his tax amount in the rent. No problem.

The statistics say that only six percent of the earning members in the country file an income tax return. Six-flipping-percent. And income tax totally contributes to only two percent of the revenue of the government. I mean, why even bother? Why target us? For the two percent, I have to forgo a fifth of my income?

Also, for everything I buy, I pay the _Goods and Services Tax_. And {{< smallcaps "GST" >}} is a good thing; make no mistake. It is a relief from a series of taxes like excise duty and sales tax and what not. But here is my problem: Those vendors who never bothered paying tax before in the lifetime of their business, now have a flashy new {{< smallcaps "GSTIN" >}}, and pay the tax. Isn't that good? No. They charge the tax on _my_ bill, by simply adding an asterisk and {{< smallcaps "GST" >}} applicable".

But hey, pay the tax; help your country grow. Why? So that the government can give away rice at a rupee a kilo, and mixer-grinders and fans and television sets and laptops and bicycles and all of those things that are resold at a thousand rupees a pop. So that the government can waiver all loans taken by farmers---waivers that even the farmers don't really get. So that the government can give away benefits to those below the poverty line. Free medical aid, and subsidised seats in colleges and subsidised cooking gas, and more unreserved coaches in trains...

And because these freebies are also to be given by private organisations (such as hospitals), I am the scapegoat they recover it all from. Great. Let's recount:

- I pay income tax.
- I pay taxes for food.
- I pay taxes for travelling by the train or the bus.
- I pay taxes for my haircuts and sanitary pads.
- I pay for the farmer loan waivers.
- I pay more money for food items, because, "Hey, poor farmers!"
- I pay the unsubsidised amount for cooking gas (because my income indicates "luxurious" life, whatever that means).
- I don't get the subsidised food.
- I don't get tax exemption on my home loan because the builder sells the flat at a lower rate on paper so that he has to pay a lower tax.
- I don't get the mixers and fans and televisions, which essentially I paid for as tax.
- I ride on potholes with roads here and there.
- I pay for my son's education because:
  - I should be able to afford paid education.
  - Parents of sons are considered wealthy (Y‑chromosome {{< smallcaps "FTW" >}}).

Now, I could get myself a below-poverty-line card. I just need another fifty thousand rupees. No additional taxes. All-offline transaction. But who has fifty thousand rupees after all this?

The point with my society is, if I wear a thread around my torso, or I carry a Y-chromosome in my {{< smallcaps "DNA" >}}, or have a job that gives me a chair and a desk, or a house that has a flat roof, or a vehicle on whose seat my rear can rest, or a wallet in my pocket, or English on my tongue, or a {{< smallcaps "PAN" >}} in my wallet, or a phone which lets me do cashless transactions, or glasses on my nose, or no cap on my head branding me a common man, I am a common man who can afford it all: I can afford to pay taxes, I can afford the fuel surcharges, I can afford price hikes, I can afford paid education, I can afford a seat in a college after the reservations, I can afford private "medical management", I can afford an unsubsidised meal, I can afford a home loan, I can afford an investment with the Insurance Corporation, I can afford a flipping underwear, irrespective of whether I can afford it or not. What I _cannot_ afford is to talk about it.

My net worth is a _negative_ six-and-a-half million. But I am not below the poverty line. I pay taxes. You're welcome for my blood, Economy.