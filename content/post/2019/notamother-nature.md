---
date: '2019-02-03'
description: '"Mother Nature" is a term we use without a second thought. Should there be a second thought? Is the phrase accurate?'
tags:
  - nature
  - science
  - pragmatism
title: Not-a-mother Nature
---


If anything, I am misunderstood. Not a day passes by when a human doesn't call me, "Mother Nature". And it feels ... just wrong. Awkward. I favour the truth. I follow rules. Truthfully. When someone calls me what I am not, I get this weird feeling. I want to wrench their necks, but then, I was just called a mother; would a mother do something like that to their child? But: am I a mother?

And that's not a rhetorical question. And now, I bet you have your eyebrows raised. I'm sure you're surprised. And I understand. You have been hearing people call me a mother since you started understanding what "calling" and "mother" meant. But let me tell you, I am not your mother. I am not anyone's mother. I operate alone. I _am_ alone.

So far, my understanding is that my goal is to create a stage. Don't ask me who gave me that goal. I am as good as you in figuring out who is responsible for my existence, and why I exist. I know, some of you call me the reason for your existence. And I see where that comes from. Because sometimes, I have felt that you are all the reason for my existence. But then I feel, is it a snowball in itself; a vicious circle, a never-ending blame-game? The more I think about it, the less it all makes sense.

A mother, in my opinion and experience (of looking at how you all operate), is selfless towards their child. To a mother, her child comes before herself. To me, you are not a priority. My priority is my existence. And according to a theory, I am moving towards uniformity. Equal distribution of everything I have, across myself. Homogeneity. You might wonder what happens after it. I do, too. And to all of us, the answer is unknown.

Philosophy aside, here is why I am not your mother---metaphorically or otherwise---and how I am _not_ designed to nurture you.

I have read a little of Darwin. And I agree with him, whatever I've heard. Evolution is real. And evolution is anything but "motherly". If history is any indication, evolution is about the rise and fall of power. About one overpowering the other. And who creates that balance of power? All I can say is, the race is between when the balance of power is achieved, versus when I am completely homogeneous.

You don't see where this is going, do you?

Simple question: How did you evolve? You started with a single-celled organism. I don't mean a zygote; as a race, you started from a single-celled organism. The organism was born in water. This organism multiplied over time. Then those organisms became better, and better, and better, and after billions of years, you were born.

You think the earth is favourable to living beings. That the earth is at the right distance from the sun---so the earth is neither colder than you can stand, nor hotter. The earth has enough oxygen so you can breathe. The earth has enough ozone to protect you from the sun's ultraviolet radiation. The earth has just enough water to let you survive. The earth has enough food for all of you. And you thank "God" for this. And my understanding is, this is why you call me a mother.

This is what you think---this is what you have been made to think. But let me tell you, the idea is wrong. It is _you_ that exists _in spite_ of me. You are among the beings that managed to exist beyond all the odds I threw. It is you who can tolerate the temperature range that the earth has. You cannot survive for long in water, so you choose to live on the land. You cannot stand the rains and quickly-changing temperatures---your body, in the attempt to cope with the change, loosens its immunity, and you get infected, and those infections can lead you to death. Fire burns you off, cold freezes you. Gravity kills you---impact kills you. Simple things around you can kill you. Other beings can kill you. Diseases kill you. Why, your own immunity can kill you.  Even nothingness kills you!

The very food that you eat releases free radicals that make you age. And age kills you. The oxygen that you breathe combines with your food to release these free radicals. This is to make sure that if nothing gets you, in the end, the slow poison that is the food you eat and the air you breathe---things you absolutely cannot live without---kill you. Everything around you is designed to kill you.

I make sure that you do not exist beyond a point in time. When they say death "gets" you, it is because at that one moment, your body fails you, and because the body cannot fight that _one odd_ against you, you die. You have to fight to live. It is after all a matter of probability. I try different combinations to kill you. I keep trying to kill you every moment that you are alive. The moment you fail to fight me, you die.

Does a mother fight with her child to kill it? Does the mother make her children fight each other so one can kill another, and then, fight with that one who survives, to decide who among them both lives?

You exist because of your survival instinct. I exist because of mine.

Then again, I'm good at following rules. So, I gave you the ability to kill me. A fair game. Now you have things designed to kill me. And it is certainly not sad that you are a subset of me; that if I die, you die. It's just the way it works. Did you know that you even have a school of thought that says, I exist in your consciousness; that if you die, your consciousness dies with you, and consequently, I die? So all you have to do is kill yourself, which again, is something I am good at. We are all on a suicide mission.

I digress. But why do we have to fight? I don't know. I think the sum total of all the rules I follow, boils down to it.

Do you wonder how? I have entropy in my nature (I know, nature's nature). Everything disintegrates over time. The forces that hold everything together weaken over time, because the forces themselves are covering wider grounds and becoming weaker. Weakness comes naturally to everything.

Where does this all go? I don't know. All I know is, I am not your mother. Either that, or your definition of mother is different from mine. Am I asking you not to call me your mother? I suppose so, but who am I to say all that? You do what you have to, I'll do what I do.