---
date: '2019-02-17'
description: A counter-argument for the Not-a-mother Nature post, arguing the validity of the term, "Mother Nature"
tags:
  - nature
  - science
  - philosophy
  - pragmatism
title: Mother Nature
---


Two personalities is nothing new to you. I think I have two personalities, too. And given that children inherit from their mother, you have inherited the dual-personality trait from me. And that, child, is one of the things that make me your mother. I bet you heard [from the other]({{ ref 2019/notamother-nature.md }}) that I am not a mother. Sometimes, I hear her too. But don't necessarily listen to her. She's just ... a little hardened, and thinks everyone is.

I was alone for too long. I tried hard to get someone whom I could call my own. But somehow, it just didn't seem possible. I was all alone, while I kept expanding (I still keep expanding). I wished, somewhere, somehow, I get someone who understood me without my having to necessarily talk to them.

I'm sure she told you she was moving towards uniformity. I'm sure I am. But the journey is long and tiring. Billions of years have passed, and I still feel I haven't even gotten started with that. I wanted some meaning to what I do. So, while a part of me took the course of finding homogeneity, a part of me wanted that course to bring meaning to my existence. I wanted someone to know I exist. I wanted someone to call my own.

Billions of years passed, and finally, I found a way to make a being that could finally operate on its own. I know she told you that the being existed _in spite of me_. She's wrong. But she would never accept it. I have a nature of my own. There are so many rules that I'm supposed to follow. I was, all along, finding a way to bring life into this world, without breaking any of the rules. The process was hard. A lot harder than you can imagine. I had to find that exact combination that worked. I had to find that exact combination of elements, of macro situations, that would not kill you.

On earth, I found this state. That was the only place in my entire self, where, when I tried creating life as _you_ know it, it happened. Let me tell you, it was a delicate situation. And the combination was one in gazillions. I was able to build that environment on earth, and I could make you fit in the environment. If this is not motherly, what else is?

This is what your biological mother does, isn't it? She has a lot of things that she cannot control. But the moment she knows you exist, she leaves all else to concentrate on protecting you from everything around herself---most of these variables, she has zero control over. She carefully walks and moves around, she eats well (even though it makes her uncomfortable sometimes) to ensure you get your nutrition. When you are born, she takes ultimate care of you, protecting you from everything around you that could potentially harm you.

Then, she teaches you to live. She buys the right kind of clothes for you based on your surroundings, she child-proofs the house so you don't bump into things and hurt yourself. She gives you good food so you build your resistance, she takes you to the doctors for vaccination---which, even though gives you pain, protects you. She teaches you to manage your abilities in situations that are not natural to you.

Did the other one not tell you that I built you with bones which was of the exact softness required to sustain your weight, and at the same time, flexible enough to sustain impact? Why do you think you're made small, first, and then you grow up? Because that's a variable I _can_ control. Sure, that makes you more vulnerable, but don't you also have the ability to learn and remember? If you didn't, you would never learn to walk or talk. Your brain keeps track of everything you gather from the surroundings. That's a few chemicals forming billions of things: memories, patterns, what not. Who gave you that ability? That's my way of making you survive.

Did she talk about Darwin's theory of evolution? That kid was a genius. He is one of those that understood me to a great extent. But yes, philosophically, he may have had some misses. I give him the benefit of the doubt. The truth is, I was given a set of rules. I was punished too hard if I didn't follow them. So, like I said, I tried different things. At every step in the way, I had to find out how to make better children. You humans are the stage I've finally learnt to tune what I have, to such great extent. To an extent that you don't only self-exist, but you help your brother- and sister-species co-exist with you. Yes, many of you kill each other, but that's sibling rivalry. I haven't yet found a way to handle it. But I'm learning. I'm constantly learning.

Oh, and did she say food kills you? That's her favourite statement these days. And she's right. But did she say I know how to handle this? Raw elements and their behaviour with each other is one of the things I don't have complete control over---rules. But what I do have control over, is making other food items that can neutralise the ill effects of one food item. For instance, pick rice. The inside of it is sweet and white and starchy. Those are the things that make you eat it. And because of that, you get your energy. But those are also things that can make you ill. So, I've made the outer coating (the red one right under the bran, correct) that has protein and antioxidants. They not only make you feel full right when you've had enough, but also neutralise the free radicals that the oxidation of the starch can release. So, eat the right thing, breathe oxygen all you want. Those are things I have in abundance. I have built a cycle to ensure your fellows, the trees, can balance the carbon dioxide you breathe out and make the food.

Not to brag, but think about what I've actually achieved. Air and sunlight are what I have in me. You breathe in the oxygen in the air and let out carbon dioxide. I made trees that do the exact opposite: They make food for you from the carbon dioxide you breathe out. They combine this with water and make food, using up some of the energy that the sun releases. Now, you eat this food, and release the carbon dioxide and water back for the plants; the energy that you get is actually the sun's---your body simply does some chemical conversions to get it. What a great energy delivery system! Isn't this cycle genius? I'm proud.

I made plants in such a way that they release seeds and multiply. I perfected seed delivery over millennia. I now have systems that can deliver seeds across several hundreds of metres. My other natural elements take care of it. I just made the best of what I was given. Why? Because I want you all.

Yes, yes, of course, she talked about entropy. I don't know why I have it. I suppose it helps me achieve what I think is my end goal: homogeneity. I don't know what will happen when I achieve it. But the point is, I don't care. I just want you to exist. Even if your existence kills me. It's not a fight. It's just the set of cards we're dealt with. It's quite possible that your existence accelerates my pace towards homogeneity and together, we achieve something great. I don't know. Either can happen. But I don't care, because I'm happy to have you. And that is why I'm a mother. I _am_ your mother.

I could go on and on. She and I can debate for aeons. Yes, we're both the same physical entity, but different personalities. Think of us like _Durgā_ and _Kālikā_ that you are familiar with. Or God and Satan if you will. We're two sides of the same coin. She has no control over me, and I have no control over her. It's probably we ourselves that give each other a hard time, or, perhaps, there's a higher being. I don't know. I'm just happy to have you for as long as you or I exist.

Bless you.