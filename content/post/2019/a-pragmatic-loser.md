---
date: '2019-01-20'
description: If I could bet, I would say every place has its share of people who do nothing but make others’ life miserable. Does leaving such places make you a loser?
tags:
  - office
  - politics
  - corporate
  - it
title: a pragmatic loser
---


Work is worship. This is what I learned growing up. At that time, it was studies that was work. Then, it was defending my country. Now, I work at a multinational IT giant, as an IT Manager. Outsourced IT Services is a funny industry. While I'd heard a lot about it, I was shocked how true everything I'd heard about it was, when I saw it happen in reality.

One of these things is how it seems there are more people to monitor work than there are to actually work. Sometimes, this is not just a feeling. My team has this system of creating a "work log". They literally sit down and log everything they did throughout the week. (They even log the logging part of it.) And they each send this log to my on-site counterpart, working out of the client's office in the US. I wonder if that man actually reads the log. Either way, to me, this seems pathetic. And sad. "What did you do between 12:21 and 12:24?" "I was trying hard to get that sneeze out." "If you had to try that hard, you probably weren't getting it. You wasted three minutes of our client's time. Worth $3.25. That's 23 rupees of your salary." Cringe.

What's funny is, this man does nothing; literally nothing. Except making work hard for us offshore folks, that is. After years, he has finally managed to get a Green Card. It was a dream-come-true for him. Should he not be satisfied? Provided he's with this account. The offshore team is a group of highly efficient, driven folks. Because of us, he doesn't have to work. But the problem is, he has to show that he has work. Otherwise he would be thrown out, and he would have to find something else to do. That could be another account, or another company. But would he get this kind of a team there? Probably not.

The golden eggs should continue to fall. He should continue to pick these eggs, insert them into himself, run to the client, and pop them out in front of them. Of course, he sugar-coats the eggs. The sugar being things like the work log. I indulged him initially. But then he started getting on everyone's nerves. He would make odd demands, promise the impossible without asking anyone first whether it was possible or not, or do something utterly stupid, and then hold my team responsible for everything. He would showcase all the good to the client as though he drove it to success. And not even pass on credits to my team. Not even so much as a "Thank you for pushing crazy hours, Team!"

I built this team from scratch. I hand-picked each of them, after not only checking their technical competence, but also their attitude. I trained them to be efficient and knowledgeable. I respected everyone, and ensured that they respected each other for their contributions. This team slogs, day and night. I sacrifice my team outings and family gatherings so these kids could enjoy themselves---I support during weekends, sitting alone at home. My team takes the blame for this man's "misses"---which is another word for "blunders", around here.

He is a star. But at our cost.

He plays games with the team all the time. He wastes their time by keeping them busy with the unnecessary. All for what? To establish _his_ importance.

I felt it was time I put my foot down on such behaviour. Being professional was not going to work. These places required something beyond work. It required work to eliminate those who didn't have work. I rubbed a few flints together. A few days later, fire blew up. His spot was in danger. He was quick to pull strings and call some more of his kind within the company---offshore---to help him. They happened to be my superiors. I was called into a meeting room and asked to pull back. I refused. They insisted. They pressurised me until I backed off. Their trump card was, 'He has two children. Do you want them to suffer? What was the worst he did? Take credit for your work? Think of it as something you did for those little girls.' Seriously?

Then I decided to quit.

But then, wouldn't I be a loser? I work. My team works. He doesn't, but gets all the credit. And I have to leave?

I imagined everyone around booing. Laughing at me. Calling me names.

But then it hit me. I work because I like to. Because that's the right thing to do. I'm passionate about what I do. Of the nine hours, I'm doing something or the other for at least seven.

This man does nothing. He doesn't have any work. He wouldn't work even if he had any. So, he politicises the environment. That's all he does, and he is really good at it.

If I have to defeat him in this game, I have to work, _and_, play politics. Do I have the time or the inclination for it? And what would I gain by it anyway? There are others like him, above me, who would continue make my life hell. On the other hand, what am I losing by leaving this place? Can I not find another place that requires someone to _work_, and work there? Here, he would play no matter who is in my seat. My conscience, on the other hand, would not allow me to do what he does and get paid for it.

I feel that I should really think about myself, and only myself. I have no intention to think about what he would do---nor is that kind of thought going to help me. Let him rot in this bog. I'm born to fly. If leaving the bog to scale the limitless skies is someone's definition of a loser, so be it. Once I'm off the ground, that someone would not even be visible to me.