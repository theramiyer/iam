---
date: '2019-03-10'
description: A railway engineer decides to go to Chennai just because he is bored of his work. During the journey, he revisits a quirky aspect of his past, and discovers something related to it.
tags:
  - satire
  - south Indian
  - actor
title: an aspiring soap actor
---


There was a time I worked as a mechanical engineer with the Indian Railways. It was a boring job. I did not even have a supervisor to tell me what to do. I was a far cry away from being self-managed, so, one day, I hopped aboard a train headed to Chennai, in hopes of finding something interesting to do.

I had heard that Chennaiites were innovative folk. What I saw in Chennai was nothing less than what I had heard or imagined. And I, for one, was not in any position to compete. Not like I am a [{{< smallcaps "PWD" >}} minister who thinks too high of himself](https://youtu.be/zRhDWnGlWEk?t=29). Instead, I was going there to watch and learn.

I reached Chennai Central. When I came out, I felt lost. As a Railways guy, I automatically get drawn to tracks. Therefore, my feet led me to the Chennai Central Metro station, which I eventually descended into, later feeling a little lost, again. I could sense the tracks, but all I could see was a glass wall. Like some office. Someone later told me, the doors on the wall would open when a train arrived. True to the word, it happened. I understood that the reason behind this was to conserve the conditioned air. Brilliant. I wouldn't expect anything less from Chennai.

I boarded the Metro train and reached Vadapalani. I chose the place because it was early morning, and I was hungry. The name reminded me of breakfast. Besides, I had heard a lot about the place through films. The famous {{< smallcaps "AVM" >}} Studios stood there majestically. Sure, some say the place isn't used anymore, but I wouldn't know much about it; I am a little man from a remote village at the heart of Tamil Nadu, after all. And no, Vadapalani has nothing to do with breakfast. It's ... never mind.

A few hours, later, I found myself roaming the streets of KK Nagar. I saw a house swarming with people, a couple of generator vans parked outside, lights and shooting equipment everywhere, and a few quite familiar faces. It doesn't take a scientist to understand that a Tamil daily soap was being shot.

I took in the entire scene. I imagined my days back in the nineties when my then secret crush would come home only to watch TV with my mother and sister. I watched how she admired the actors, and how she lauded their acting skills, saying she could not understand how they managed to act so naturally. Those days, I wanted to quit college and learn to act, so she would watch me on the TV every day, and some day, if I proposed marriage to her, would accept it with uncontrollable joy. But then, it did not take me such effort at all. All I had to do was study for the Staff Selection Board exams, clear them, and buy myself a job for half a lac rupees. Her father came home and asked me if I, the government employee, would marry his "only" daughter. Of course, I accepted.

I have lived happily since, no doubt. And even today, she watches all the soaps. Sometimes, I accompany her when I don't have Internet connectivity on my phone. There are only so many offline ways for a man to entertain himself.

Anyway, on the days I watched these soaps, I've wondered how these actors manage to show so many emotions. Where did they train themselves? How many hours a day did they spend training themselves? Who trained them? How? Too many questions. Luckily, since the theme was the same, I did not have to write them all down. I just went and asked a random guy in the crowd, '_Thambi_, who trains these actors to act so well?' 'The director, of course,' came the reply. This boy seemed to know a lot, and so, I asked him if he could take me along for an audition, or tell me where these auditions happened. Luckily for me, the auditions were happening in the courtyard of the same house. I went around the house and reached the backyard.

There was a man, sitting in a cloth-and-metal chair, wearing sunglasses (quite unnecessarily, for the surrounding conditions) and a hat. He was saying, '... so, you would all tell me how long, in my ear.' He had a few sheets of paper. The candidates queued up in no time. They went to him one after the other, and whispered something in the man's ear. The man directed them to either of the two groups.

Then, he asked, pointing to one of the groups, 'Who among you all have acted before?' He picked only those who showed their hands, and asked the rest to wait in "the other room". This pool had men and women, both. And hardly anyone good-looking. This could not be a couch situation, if you know what I mean. He turned to the others, and said, 'So, do you all agree? This is some serious shit.' And he laughed to himself. Loudly.

This did not seem like much of a sophisticated selection process. When I mentioned the same to the boy, he laughed at me, and said, 'Let us do this: go in and watch them shoot.'

'Err, would they allow us in?' I wasn't sure.  
'Who do you think I am? I am the co-producer!' he said, patting my shoulder.  
'Right, and I am Narendra Modi.' I said.  
'Dude, I'm serious.' The kid wouldn't let go.  
'Yeah, me too!' I insisted.  
'Never mind,' he said, and dragged me in.

The shoot was in progress. The atmosphere was tense. The two ladies in the room were bawling and yelling at each other. I looked around for bottles of glycerine. All I could find were empty water bottles. 'Those are natural tears.' The boy almost read my mind.

'You don't say!'  
'Well, innovation, my fella.'  
'I'm sure. Wait; they didn't put any water into their eyes. So, what is it, how do they do that?'  
'Well, it's only natural they feel that way.'  
'You and I both know this is a shoot.'  
'Yeah, but the feelings are natural.'  

The kid seemed a little off. But, he didn't seem to lie. They didn't seem to be faking anything. I am sure that they were not related, nor did they see the other actors as family. It would be crazy to assume they actually felt what they were saying.

'Cut! What a perfect shot! Brilliant, folks!' The director yelled, excited. He turned around to the boy next to me, and said, 'Boss _thambi_! Done! _Innōru episode pōlāmā?_' The boy pointed to the actors.

'No, please, no. Let us wind up.' One of the senior actors (or should I say actresses) said.

The boy said, '_Appo pōlām!_'

Right before the shoot for the next scene began, one of the actors ran to the director. 'Sir, we did not discuss about this. I haven't had the chance to prepare for the next scene.'

'Sir, your role has only two sentences. Please memorise them now. This discussion is over. Now, positions!'

I could tell that the person's voice was trembling on the inside. Did these people talk like this in real life, too? I mean, come on, passion has to have a limit! I saw the boy smiling like the Mona Lisa.

The camera started to roll, and the dialogues just flowed. The emotions overflowed. I couldn't believe that I was living the scene. And suddenly, the man who was unsure, stuttered, unsure what to say. He was trying to recollect while the others retained their expressions. But he failed in the end. The woman standing next to him smashed a juice tumbler on the floor. A second ago, it was shining in her hand; now, it was gone. Shattered. She held his collar, and yelled, 'There are people struggling here, and you are unable to utter a single line, you dead maggot!' He looked at her apologetically. 'I am sorry, I will try once again.' The woman wouldn't soften. 'Yes, and you better ace it. This is not easy, what we are going through here. You may have cheated or something, but we haven't. Now, go, read your stupid two-lined part. Let us get this over with as soon as we can. Don't waste our time apologising.'

Wow, that was something. Her voice almost echoed. She was at the peak of emotions. That expression! The room got charged further. The director or the co-producer next to me did not utter a word; they did not seem to have to.

Me, I had drunken a little too much water in the morning. I had to excuse myself for a minute or two. When I was about to leave, the boy asked me, 'Where to?'

'Well, it's the nature's call.'  
'Okay, then this is the way out', he said, pointing to the other side.

What filthy ways! In urban Chennai? Seriously? I went into the house, and asked someone, 'Where is the bathroom?' The man chuckled. Never mind, I would find it myself. In a moment, I did. I unlatched the door and pushed it. it wouldn't budge. I knocked. No response. I yelled; no response. In what godforsaken place do people not respond to a 'Hello'? I tried a different bathroom. Same situation. All the doors seemed locked from the inside. And then, a man said from behind me, 'Those are nailed shut; you cannot go in.'

_Nailed_ shut? Why would anyone do that? I started to walk back to the shooting spot. On the way, I saw the man who had stuttered, tell the woman who had yelled, 'I did not cheat. I haven't eaten anything since yesterday. Remember, I had arrived a little late yesterday and the food had gotten over by then?'

The woman did not bother to respond. She just picked up her bag and her keys, and dashed out of the house. The next scene was being shot. And the air had become a little more tense.

'There is the civilised man!' The co-producer boy yelled.

'Yeah, yeah, here I am. Yay!' I feigned excitement.

'This is a place of business, my man. We have some rules here. The doors have been nailed shut. If you go upstairs, you will find a few luxurious rooms, reserved for the main characters acting the next day. The only thing that is missing is ... wanna guess?'

I didn't have to. I just looked at the boy.

'The actors are allowed to do everything they want to, here. I pay them really well, too. The only thing they don't have control over is what they eat and how much they eat. They can choose between vegetarian and non-vegetarian food, but that's the only choice they get to make. And they are not allowed to waste any food either. And then, of course, no toilets. But I shoot early in the morning. I understand humanity.'

He didn't have to say anything more. 'But ... isn't this⁠---’  
'Unethical? You heard Hat Man; he asked if they agreed. Only those who willingly agree come here.'

That's it; that was the answer! I couldn't belive this. I asked the boy, 'Co-producer _Thambi_, will you let me act, please?'

---

Update (03 May 2019): This evening, I realised that there was a similar concept shown on _Friends_ as well. Yes, Joey's audition scene. Tough luck.