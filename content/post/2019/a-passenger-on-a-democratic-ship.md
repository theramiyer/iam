---
date: '2019-02-24'
description: If a democracy were a ship, how should it work? More importantly, how does it work?
tags:
  - democracy
  - civics
  - politics
  - satire
title: a passenger on a democratic ship
---


I was born innocent. I cried like every other healthy child did. I was terrified when I was carried around by so many of what I later understood were hands. I distinctly remember the few initial moments, in which I opened my eyes and saw women throw up. The nurses were unsure if it was their pregnancy that caused it or sea sickness. The women smiled dearly either way; both of these cases were more or less the same to them. In either case, the condition was because of a choice they'd made. Both choices came with a beautifully serious set of responsibilities. And then, sometimes, there were rough days like this. The only difference was, the former was a result of the efforts of two, while democracy, the latter, was a gift from millions of us, to ourselves.

Today, I ride this ship. I enjoy this journey. I cannot compare it much with other ships, because, I've hardly seen anything else. But I have read a lot. I sometimes talk to my cousins who changed ships. Most of those I know are on ships that are similar to mine; it seems to be something that has gained serious popularity these days.

I feel superior to those who are not on ships similar to mine.

Everyone in our ship is actually taught to handle the ship. We are given lessons about the ship when we are children. Once we are adults, it is our responsibility to contribute to the running of the ship. We all, together, own the ship. Nobody is figuratively bigger or smaller than another. That is to say that even the captain is the same as the old woman who sits in the corner of the ship, weaving hats. That is how it is supposed to be. That is how it is, on paper.

We are all free on the ship. Nobody owes anyone anything. We are free to talk to everyone, to question anyone---including the captain---to worship the gods we feel like worshipping (or not worship anyone at all---in general, the ship worships everybody), we are all equal, nobody is allowed to take advantage of anybody, we are free to go from one deck to another---pretty much anywhere in the ship---we are allowed to follow our own beliefs and ways to do things, collectively called "culture", we all learn for free---and anyone who wants to learn can learn---and there is a group of people whom we can go to, if someone breaks the rules or does something to us that they are not supposed to.

Isn't that great? There is a whole, big rule book that talks about how the ship should be run, and how those on board should be taken care of. I haven't been able to read the book cover to cover, but from what I've read, I love it.

But there are things that I am not quite happy about.

I read in one of our class books, that in the past, our ship was attacked by some other ship. The other ship took control of our ship and ran it for about two centuries. During the time, the people on my ship faced a lot of hardships. But at the time, my ship ran a whole different system. There were sections in the ship that specific individuals owned, and those in those sections were ruled by the said individuals. They were all so divided that the other ship found it very easy to make these individuals fight among themselves, which later led to the other ship taking control of my ship.

A few decades later, some people in my ship woke up. They started working towards the freedom of my ship. Slowly, they united all the sections within the ship, and together, they fought off the other ship. A few decades later, we were on our own. It was under the leadership of this group of these few individuals that my favourite book was written. They, along with the more common others, set up the entire ship to follow what we follow today.

It was all great. The idea was, empower everyone on the ship to run the ship.

You see, the system is like this:

Everyone contributes to the ship's running. We call it a job. For instance, my "job" is to go around, look at how things are working, and tell the others about what is going on. Similarly, there are those who work on navigating the ship, those who watch the seas, those who take things from one corner of the ship to another, those who take care of learning, those who address concerns of those on board, those who ensure that those on board are fed, and so on. Some of the jobs are directly linked to the running of the ship (we call it _The Leadership_), and others, not. People in the Leadership work shifts. And it is us, _The Commons_, who decide who should work the next shift.

The system is, the people who apply for a job in the Leadership should show us, the Commons, that they can handle their shift. If we are convinced, we choose them for the next shift. They go to their seats and take the handover from the one currently in the seat, and continue doing what that person in that seat should do. This could be navigating, taking things from one place to another, or anything like that. When their shift is coming to an end, we do the election thing again, to choose who would be the next in shift. The handover happens, and the ship continues to run. But being in the Leadership is tough job. You have a lot of responsibilities.

Some ships are more humanitarian than the others; they have a limit on how many shifts someone can be asked to work. The ship that my cousin is on, for instance, says that a person can work only two shifts. He cannot be asked to work more than that---it is simply not possible to force someone to work their entire day. My ship, on the other hand, does not have any such limits. Everyone is free to work as much as they want. My ship also has a big bunch of those who would pretty much sacrifice their entire life to working in the ship. They work hard from childhood. They work hard, day and night---some watch cartoons, some sell beverages, some make statues, some build trains, some jest, some debate, some eat (food, of course) and sleep---to tell us that they can handle the ship. We consequently pick them for the jobs they say they can handle. (Sometimes we even pick someone only mediocre just because there is no better.) They go on their shift, and the rest of us can stop worrying about the position; they are more than happy to do that job until their last breath. Of course, there are also those who dream of getting there, working diligently towards the seat, spending sleepless nights on the deck, and end up in their deathbed without ever making it to the seat. Poor souls.

Then, there are those who have brands. Here are a few short stories:

We have an adorable Aspiring Leadership Boy, who watches a lot of cartoon. His great grandfather was among those fighters of the Old Days, when my ship was under the control of another. So this Cartoon Boy's family has been in the Leadership frequently. Great Grandpa was the first captain of the Free Ship. Then his daughter took over the responsibility. Then came her son. Now, we have the Boy, who cannot wait to start serving the ship as the captain. This Boy and his friends own a brand. When the ship is running under this brand, everything is given a stamp. The doors are branded, the eateries are branded, the pathways are branded---they are all branded after this boy's family name.

Then, there was another woman, who was unquestionably a great leader. She did things really well: the women and the children were taken good care of, learning was given importance to, there was order on the ship, and so on. People loved her. And so, she was happy---who doesn't appreciate love? Her friends in the Leadership decided to make her a brand, so that people remembered her, and the great days she brought. So, her face was stuck on food, her face was on the doors, her face was shown on Entertainment Systems, her face was everywhere.

There was also another grandpa, who wore dark glasses all the time and liked a couple of colours more than the other colours---he thought they symbolised something. So, he painted little carts with his colour, he built beautiful pathways throughout the ship, and even got a spot in the ship named after his initials.

The sad part is, there are those among The Commons, like me, who cannot see all the sacrifices that these men and women make. I go on TV and make a lot of noise and ask other ships to attack mine, while having no idea about what a tough life it is for those on the edges of my ship. I know what damage another ship hitting my ship would cause to the both of our ships. But still, I challenge other ships to hit mine to see what happens to them. The reality is, my job is to get more and more people to listen to me. I am an attention-seeker. I would seek attention even at the cost of my ship. Even if it means that I die in the process.

This is my life on my ship. If you see a problem, come, fix it. You have all the rights to. We are all equal, remember? But first, you would have to take care of me, my supporters, the Leadership that you're unhappy about, their supporters (some of whom would be in your own family, and friend circles). But you're welcome to try.

More power to you!