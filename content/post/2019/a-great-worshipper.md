---
date: '2019-03-03'
description: The story of one who worships gods with all his heart. His faith is unquestionable, his devotion is deep. But...
tags:
  - satire
  - religion
title: a great worshipper
---


I happened to visit quite a weird place a few years ago, called, _Bhōjanālayam_. Apparently, this is the best place to find gods. I grew up without going to _Ālayams_. I don't know why. My parents simply did not like _Ālayams_. Or any place of worship for that matter. I am not like my parents. I'm quite progressive. I embrace the gods. I want to know them. I want to praise them. I want to worship them.

I want to worship them so much that they come down and meet me. I want to be able to tell others that gods visited me. And of course, share the message of the gods with others. I want the gods to say good things about me to people. I want my name to be written in the chronicles. I want people to remember me as _The Greatest Worshipper_.

You must be wondering what I do. I am a _Leader_. I lead gangs. I make them do good things. And to do good things, sometimes, I have to do bad things. People don't like me doing bad things. Obviously, right? But I can't quite help it; this is just how I live, how I survive. My world needs people like me for balance.

One day, while leading the gang into what I call a revolution, I hurt myself. Not in the bleeding way or anything---just hurt. I could not fight anymore. I felt a little dizzy and lost control. I fell to the ground and bumped my head on some nasty furniture. I got a concussion. I was taken to the doctor, who asked me what happened. I told her that I kind of fainted.

The doctor smiled and asked me to take rest. I did. When I was leaving, I asked the doctor, 'Doc, what caused this? How do I avoid this in the future?' The doctor flashed her usual smile and told me, 'Well, your blood pressure went down and your brain did not get enough oxygen. It's nothing serious. Take rest for a few days.'

'Okay, but how do I prevent this from happening again?' Why would I leave it off like that? I have paid for this consultation. I will get all the information I need and only then, leave. That's how I am. The doctor didn't mind my persistence, though. She said, 'Well, the one thing that could've saved you is Sugar.'

I realised, Sugar is some sort of god. I wanted His blessings. Only His blessings will save me from all this. Only with His blessings will I be able to fight my enemies in the future without fainting or looking like a loser. So I started asking around where I will find Sugar. Some said I will find Him in the grocery store. Some said restaurant. Some said coffee shop. Some said we could find Him _everywhere around us_, including fruits.

One day, someone told me, Sugar with other gods is better than Sugar alone. Someone said there was this god called _Paneer_. Some said Sugar and Milk were great friends and so on. It was chaos. So, I heard about this _Rasōī Bābā_. He lived in some always-busy building. People said, he would be found wearing white everything. White shirt, white trousers, white apron, white hat ... so I went in search of him.

After three weeks of searching, I found him. He was sitting and sipping what he called his elixir---some single-syllable thing ... I forget the name of it. Something like _Khāi_ ... er, no. I think it was _Chāi_, yes! Anyway, he says it gives him great powers. It even relieves him of ailments like the headache and all. So I went and asked him, where I would find Sugar. He laughed. Asked me, 'What would you do with Sugar alone? Haven't you heard? Sugar works with other gods to create some crazy stuff.'

I was confused about what to do. I mean, here I was, finding it hard to find one god, and he is talking of other gods who "go well" with sugar? Who cares? I want only one: Sugar! But no, he wouldn't listen. He handed me a book he said made him what he is today. He said it was a great book. It was called _Master Cookbook_. Man, I could feel its importance by just holding it in my hands! It was heavy. It seemed precious. Serious.

So I took it home. I started reading it. It spoke about how to get different kinds of gods together to make god-combos, who will give you superpowers. It even told us where to find those gods. My respect for this book grew so much, I made it the official Guide Book of my gang. During induction, or when I have to settle disputes, I make the members of my gang take an oath on the book. I read the book cover to cover during the Holy Month of my gang---every gang has one of these months. Saboo's, for instance, is two months after mine. We place this book on the altar and offer flowers and fragrant water to it. I even apply vermilion to it. I respect the book a lot. I don't like it when people touch the book or handle it playfully.

I take the book along with me when I travel. I have even gotten little versions of it printed in micro-font, so that I can carry one in my pocket. Yeah, it's inconvenient---the little letters---but it doesn't matter. I know the book by heart now. I carry it so I know I carry the divine power with me. My wife took it and picked out some quotes from it to print them in a smaller book with a cute cover and form factor: a pink, glittery, little, chunky thing. My daughter uses it as a keychain, even. I have even printed pictures of the combined gods from the book, framed them and hung them around my house. Some of the members of my gang have printed out texts from it to frame and hang them on their walls.

Life is good, now. I mean, I read the book when I wake up, I read it before I go to sleep. I read it over and over again. Now, I even know what god appears in which chapters, and does what on what page. It's been years since I first met _Bābā_, and since I got the book. He's my spiritual guru, and what he gave me is the symbol of power, of life, of everything one desires. What is in the book are solutions to so many problems in life. That is why I read the book so often.

Except, something disturbing happened this evening. I met my brother. He is exactly like my parents. He thinks he can make god combinations at home. He is so full of himself. He asked me, 'What do you do with the book?' I said I read it. 'Have you tried any of what is mentioned?' Why would I do that, right? I'm not arrogant like him. I respect the gods in the book. I believe I should not even touch them with my dirty hands. He asked me if I fainted again. Sure, I still faint once in a while. I remember Sugar at those times after I recover. But my brother wouldn't listen. He smacked me and asked me to eat Sugar. Such blasphemy, I say! _Eat_ Sugar?! Atrocious! God forgive him.

And then, as if all he did wasn't already enough, he doubts and questions my belief. He says reading the book cover to cover or knowing it by heart is not going to do anything. Who says? Not like recovering from fainting is some natural process, you moron! If you don't have the blessings of Sugar, you will never wake. And you don't get His blessings so easily. You ought to know where He goes, whom He likes and all that. You should worship him. You should beg for His blessings. You should chant his name a hundred and eight times every day. Blessings do not come easily. Such idiots people are. Mucking infidels.

The lesson is, don't be like my brother. Be like me. Compete with me. Let's see who is a better worshipper. Let's see whom Sugar chooses. Hail Sugar! Hail _Rasōī Bābā_.

I should end this with a beautiful quote, so here goes:

> Sarsōn dā sāg te makkē di rōti.

That means, 'Beautiful and happy life.' (This book has one whole chapter dedicated to it.) And might I say, what a beautiful way to say it. All thanks to _Rasōī Bābā_ for giving me this beautiful book of beautiful things to say and chant. God bless him.