---
date: '2019-01-27'
description: Credit card salesmen are seen as a disturbance. What if there was a way to handle them without being directly mean?
tags:
  - finance
  - sales
  - credit card
  - shopping
title: an unusual credit card guy
---


Since childhood, the first thing that comes up when we talk finance at home is how not to get a credit card. Credit cards are seen as demons in my family. I'm sure they are in some of yours, too. 'Credit cards will drown you in debt. You will never be able to maintain any bank balance.' I was hard-wired against credit cards.

So, every time I received a cold call from these credit card salespeople, I would tell them I didn't want a credit card. I would sometimes yell at them, demanding that they told me where they found my contact details. I even tried one of the "smart" ways of asking them if it was okay if I didn't pay the bill. No matter what I did, I would get a call from one or the other card vendor every month. It got onto my nerves.

But then, on a visit to my bank one fine day, I decided to go for a credit card that my bank offered as part of the "Salary Account" package. It was a plain-and-simple free-for-lifetime credit card. Fine for a rainy day. I never really used it; it just lay in my wallet waiting for that rainy day.

A couple of years ago, this credit card sales mania began in India. Or so it seemed. Wherever I went, there was someone, asking me, 'Sir, do you use a credit card?' I would tell them I did, and they would, passionately, tell me how hopeless my credit card was, compared to what they were offering. I would be told that I would be given full lounge access at airports, free accidental death insurance (I know!), free medical insurance, twice the reward points at purchases from "partner merchants", probably even an island named after me, if I registered for their card.

At every mall I went, at every hypermarket I visited, why, even at the railway station, or the bus stand---probably the only exception being a roadside _pani-puri_ stall---I would be stopped by someone holding a stack of forms, wearing a black shirt and a tie, combed hair and polished shoes, asking me, 'Sir, do you use a credit card?' If only smashing someone's nose in public was legal!

One day, out of boredom, I indulged one of the salesmen from American Express. He explained to me a lot of (useless-to-me) things about the card he was selling. I did not travel much, and if at all I did, my home town was not even four hundred kilometres away (which meant that I never took a flight to the place), I did not eat at premium restaurants---obviously, because I could not afford them (nor was it _necessary_), I did not shop groceries or clothes for five lac a year (because my middle-class family is only four people), I had a decent medical insurance plan that my workplace paid for, an accidental insurance that came for free with my bank account, and so on. In short, I had everything I needed, and didn't want anything that I didn't need. A boring, perfectly balanced, sane life worthy of envy of many.

But I liked the way the card had been designed. I gave him details I wouldn't give even to my long-time girlfriend, and registered. Of course, you could say I registered for the time this man had spent, as well. In all honesty, though, card design aside, I registered for the card as a way to pay him back for what he had taught me. I got from him a couple of copies of the pamphlets he used, and came out of the hypermarket.

Next, I went home and read every word of the pamphlet. I also created a form with Lorem Ipsum fine-printed on it, for an _American Excess_ International card (also called {{< smallcaps "AMEX" >}}, but founded by me---no approvals; who was going to get them filled anyway?). I saved it as a {{< smallcaps "PDF" >}} document on my cloud drive. All done, I was now equipped to handle any credit card salesman, anywhere.

Yesterday, I went to a nearby supermarket. While I was picking some juicy tomatoes, a saleswoman interrupted me. 'Good afternoon, sir, do you use a credit card?' I smiled back. 'Good afternoon, ma'am, do _you_ use a credit card?' I maintained the same polite tone. I had no reason to disrespect someone who was doing their job. She chuckled.

'Sir, we are offering you a premium credit card that⁠---'

'Yes, and what I am offering you is much better. I offer you an {{< smallcaps "AMEX" >}}, which gives you free access to⁠---'

She chuckled, bowed for a fraction of a second, and said, 'Thank you, sir.' and walked away.

I followed her through the aisles. 'Ma'am, if you could spare me only one minute. I can convince you that your entire life depends on this card. Tell me, what is your monthly salary?'

She kept walking, her head lowered.

'Are you married? Do you have children?' She quickened her pace. 'No? Do you travel? My card can give you free access to the lounge at all international airports, once every three months! I mean think about it, coffee with your crush or boyfriend or your husband there, all for free. Premium coffee. Free lunch. Three times the points when you buy an air ticket using our card.'

'Sir, no, sir. I don't need a credit card.'

'I disagree, ma'am. Tell me, how many times do you buy clothes? Do you not even buy clothes for five lac a year? What about jewellery? Do you not even buy jewellery for five lac?'

She kept walking through the aisles, and I kept following her. Shopping had frozen across the store. I had everyone's attention. 'I am not even paid five lac a year, sir.'

'Okay, I'll talk to your manager. And besides, I have an {{< smallcaps "EMI" >}} offer for you as well. You also get points on every purchase, which you can use to pay back the bill. You get one point for every fifty rupees spent! And do you know the best part? There is no annual fee on the first year! If you make four transactions of a thousand rupees each, every month, you get a bonus of a thousand points. Twenty thousand points give you a ten-thousand-rupee-worth Shamazon gift card. You can even choose Slipkart. You get a flat twenty percent discount when you dine at premium restaurants. If you use FakeMyTrip---'

'I don't---'

'If you do, you get discounts up to three thousand rupees on flight tickets and twenty-five percent off on hotel bookings.'

'I don't need the card, sir.'

I could tell she was freaking out. But I was not about to let go. I wanted her to call her manager. I wanted to talk to this manager. I wanted them to feel they were on slippery grounds. Any lawyer who does not enjoy that feeling? 'If you collect forty thousand points, you get a free Bluetooth speaker. If you want, you can instead choose a ten-thousand-worth travel plan for your family, or a cash credit. If you collect thirty thousand points, you get a Manishq gift voucher of seven thousand rupees.'

She continued to pace, now exhausted.

'Making forty thousand points is as simple as spending six lac and eight thousand in a year.' I sucked in some air. 'Ma'am, I have the form with me. If you could only fill the details---'

She stopped. Turned around, and yelled, 'Hey! I don't need a card! If I wanted one, I am learned enough to approach a bank. You are nobody to tell me what I need. I don't need any stupid "offer". Stop chasing me now. Is this how a civilised man behaves? Stop disturbing me! I have my own things to do.'

I looked around. People were watching, most of them amused. I smiled back at them. 'Let me know if any of you needs that form.' This time, there were audible chuckles. I turned to the saleswoman and said, 'Didn't like the taste of your medicine?' I could see her eyes moisten. In the heat of the moment, I had ignored the fact that I was humiliating her at her workplace. I felt sorry for her. I smiled at her in a failed attempt to make her feel better. The manager had heard the commotion. He gave me a dirty look. I responded with a cocky smile. Within seconds, things fell back to normal. I could feel a pseudo-feminist's gaze drilling the back of my head, 'Would you behave the same way with a man?' Well, sure, but are you asking me this because I'm a man?

At the exit, I was approached by a couple and was asked for the form. I smiled and nodded, 'It's not a nice thing to do. What I did was to let off some steam. Don't fall to my level.'

My days run on mixed feelings; this was just another.