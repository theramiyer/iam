---
title: About
---


I take long walks. I go on long bicycle rides. I see. I think. I argue. I question. I fight. I understand. I'm another voice: I'm the voice of thought. I'm the Devil's advocate. I'm the God's reason.

That said, all of the content on this site is fictitious. Any resemblance to any incident or person (living or dead) is---unfortunately---coincidental, unless otherwise mentioned. Not that I churn out great fiction anyway.

It is also impossible to feel exactly how someone else feels about a certain stimulus. After all, that is what makes us different. But it is fun to think that you know what the other person is thinking.

This site is an attempt to put myself in the shoes of the individuals that I have met---these individuals and things that have made me think. Secondly, I also intend to keep this site open to those who would like to [submit their story to me](/contact/) (if at all). Attribution and privacy will be balanced based on what the sender asks for. I hope you enjoy reading these pieces and [send my way your constructive criticism](/contact#contact-form).