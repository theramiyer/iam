---
title: "Contact"
---

Hello, there!

_I am …_ gives a voice to the unheard. Of course, this is not some sort of campaign or a promise that millions will hear your voice—they will not: the site simply does not have that reach. In fact, I am even surprised you are here, reading this. However, it is a legitimate outlet. If you would like to share your story with the world, this site is a good context. Submit your stories to be published here, of course, with modifications in order to retain the theme and the style.

Now to some serious—but important—stuff, in plain English, if you choose to send me content (if you are just writing a note to me or sharing feedback, you can [skip it](#contact-form)):

I respect privacy. Therefore, it is entirely your choice to enter your name or contact information. All I really care about is the story you would like to share. If you enter your name, I will attribute the piece to you. If you enter your email address, I will send you a thank you email, and unless you ask me to, I will not publish your contact information.

This website is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/), and therefore, anybody who reads the content you share on this site has the right to utilise your content including by sharing and modifying it. If you choose to submit your content, you agree to allow me to apply the same license to your content as well. It will be assumed that you are the owner of the content you share, and you are responsible for any laws you breach—you absolve me of all liabilities pertaining to your content, including the liability to actually share your content on this site. Also, according to the license, since I am only using your content, I will share it non-commercially; I will not sell your content. I will not be able to pay you for your content, either. I will not take credit for your work. (If you do not give me your name, I will attribute the piece to Anonymous.)

Agreed? Here is the form.

{{< contact >}}
